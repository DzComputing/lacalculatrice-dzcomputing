#ifndef CALCULATRICE_H
#define CALCULATRICE_H

#include <QWidget>

namespace Ui {
class calculatrice;
}

class calculatrice : public QWidget
{
    Q_OBJECT

public:
    explicit calculatrice(QWidget *parent = nullptr);
    ~calculatrice();

private:
    Ui::calculatrice *ui;
public slots:
    void on_bouttonResultat_clicked();


};

#endif // CALCULATRICE_H
